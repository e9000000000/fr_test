from django.urls import path

from .views import MailingViewSet, ClientViewSet, ProcessMailingView


urlpatterns = [
    path(
        "mailing",
        MailingViewSet.as_view(
            {
                "get": "list",
                "post": "create",
            }
        ),
    ),
    path(
        "mailing/<int:pk>",
        MailingViewSet.as_view(
            {
                "get": "retrieve",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    path(
        "clients",
        ClientViewSet.as_view(
            {
                "get": "list",
                "post": "create",
            }
        ),
    ),
    path(
        "clients/<int:pk>",
        ClientViewSet.as_view(
            {
                "get": "retrieve",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    path("process_mailing", ProcessMailingView.as_view()),
]
