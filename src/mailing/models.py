from django.db import models

from .validators import validate_phone, validate_timezone


class Mailing(models.Model):
    start_datetime = models.DateTimeField(verbose_name="mailing start datetime")
    end_datetime = models.DateTimeField(verbose_name="mailing end datetime")
    # period of time of day when message will be sended to client (client timezone)
    awailable_time_start = models.TimeField(
        verbose_name="awailable sending time start",
        null=True,
        default=None,
    )
    awailable_time_end = models.TimeField(
        verbose_name="awailable sending time end",
        null=True,
        default=None,
    )
    text = models.TextField(verbose_name="mailing text")
    # send messages only if client mobile operator code and tag match
    mobile_operator_code_filter = models.TextField(
        verbose_name="mobile operator code filter"
    )
    tag_filter = models.TextField(verbose_name="tag filter")

    def __str__(self):
        return f"{self.pk}: {self.text}"


class Client(models.Model):
    phone = models.CharField(
        verbose_name="phone", max_length=11, validators=[validate_phone]
    )
    mobile_operator_code = models.TextField(verbose_name="mobile operator code")
    tag = models.TextField(verbose_name="tag")
    timezone = models.TextField(verbose_name="timezone", validators=[validate_timezone])

    def __str__(self):
        return f"{self.pk}: {self.tag} {self.phone}"


class Message(models.Model):
    WAITING = "WAITING"
    DELIVERED = "DELIVERED"
    UNDELIVERED = "UNDELIVERED"
    created_at = models.DateTimeField(verbose_name="created at", auto_now=True)
    status = models.TextField(
        verbose_name="status",
        choices=(
            (WAITING, WAITING),
            (DELIVERED, DELIVERED),
            (UNDELIVERED, UNDELIVERED),
        ),
        default=WAITING,
    )
    mailing = models.ForeignKey(
        verbose_name="mailing",
        to=Mailing,
        on_delete=models.CASCADE,
        related_name="messages",
        related_query_name="message",
    )
    client = models.ForeignKey(
        verbose_name="client",
        to=Client,
        on_delete=models.CASCADE,
        related_name="messages",
        related_query_name="message",
    )

    def __str__(self):
        return f"{self.pk}: {self.status} to {self.client} {self.created_at}"
