import logging

from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.request import Request
from drf_yasg.utils import swagger_auto_schema

from .serializers import (
    MailingSerializer,
    MailingDetailsSerializer,
    ClientSerializer,
    MessageSerializer,
)
from .models import Mailing, Client
from .utils import process_mailing, process_mailings


logger = logging.getLogger("mailing")


class MailingViewSet(ModelViewSet):
    def perform_create(self, serializer):
        super().perform_create(serializer)
        process_mailing(serializer.instance)

    def get_serializer_class(self):
        if self.action == "retrieve":
            return MailingDetailsSerializer
        else:
            return MailingSerializer

    @swagger_auto_schema(
        operation_id="create",
        operation_summary="create new mailing",
    )
    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        mailing = self.get_object()
        logger.debug(f"{mailing.pk=} created")
        return response

    @swagger_auto_schema(
        operation_id="list",
        operation_summary="list of all mailings with statistic",
    )
    def list(self, request, *args, **kwargs):
        logger.debug(f"retrieved list of mailings")
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_id="details",
        operation_summary="info about specified mailing and related messages",
        responses={404: "not found"},
    )
    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        mailing = self.get_object()
        logger.debug(f"{mailing.pk=} retrieved")
        return response

    @swagger_auto_schema(
        operation_id="update",
        operation_summary="update mailing",
        responses={404: "not found"},
    )
    def partial_update(self, request, *args, **kwargs):
        response = super().partial_update(request, *args, **kwargs)
        mailing = self.get_object()
        logger.debug(f"{mailing.pk=} updated")
        return response

    @swagger_auto_schema(
        operation_id="delete",
        operation_summary="delete mailing",
        operation_description="also delete related messages",
        responses={404: "not found"},
    )
    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        mailing = self.get_object()
        logger.debug(f"{mailing.pk=} deleted")
        return response

    queryset = Mailing.objects.all()


class ClientViewSet(ModelViewSet):
    @swagger_auto_schema(
        operation_id="create",
        operation_summary="create new client",
    )
    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        client = self.get_object()
        logger.debug(f"{client.pk=} created")
        return response

    @swagger_auto_schema(
        operation_id="list",
        operation_summary="list of all clients",
    )
    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        logger.debug(f"retrieved list of clients")
        return response

    @swagger_auto_schema(
        operation_id="details",
        operation_summary="info about specified client",
        responses={404: "not found"},
    )
    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        client = self.get_object()
        logger.debug(f"{client.pk=} retrieved")
        return response

    @swagger_auto_schema(
        operation_id="update",
        operation_summary="update client data",
        responses={404: "not found"},
    )
    def partial_update(self, request, *args, **kwargs):
        response = super().partial_update(request, *args, **kwargs)
        client = self.get_object()
        logger.debug(f"{client.pk=} updated")
        return response

    @swagger_auto_schema(
        operation_id="delete",
        operation_summary="delete client",
        operation_description="also delete all messages sended to this client.",
        responses={404: "not found"},
    )
    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        client = self.get_object()
        logger.debug(f"{client.pk=} deleted")
        return response

    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class ProcessMailingView(APIView):
    @swagger_auto_schema(
        operation_id="send",
        operation_summary="send messages to clients",
        operation_description="send messages to clients",
        tags=["send"],
        responses={200: '{"success": 1}'},
    )
    def get(self, request: Request, format=None):
        process_mailings()
        return Response({"success": 1})
