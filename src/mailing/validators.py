import re
from zoneinfo import ZoneInfo, ZoneInfoNotFoundError

from django.core.exceptions import ValidationError


phone_regex = re.compile(r"^7[0-9]{10}$")


def validate_phone(phone: str):
    if not phone_regex.match(phone):
        raise ValidationError(f"invalid phone format {phone=}")


def validate_timezone(tz: str):
    try:
        ZoneInfo(tz)
    except ZoneInfoNotFoundError:
        raise ValidationError(f"invalid timezone {tz=}")
