import asyncio
import logging
import json
from zoneinfo import ZoneInfo

from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils import timezone
from channels.db import database_sync_to_async
import aiohttp

from mailing.models import Mailing, Client, Message


logger = logging.getLogger("mailing")


def is_time_awailable(mailing: Mailing, client: Client) -> bool:
    """is now time when we awailable to notify specified client"""

    client_time = timezone.make_naive(timezone.now(), ZoneInfo(client.timezone)).time()
    result = bool(
        mailing.awailable_time_start is not None
        and mailing.awailable_time_end is not None
        and (
            client_time < mailing.awailable_time_start
            or client_time > mailing.awailable_time_end
        )
    )
    return not result


async def try_to_send_message(message: Message, client: Client, mailing: Mailing):
    """
    try to send message
    if success: change message status to DELIVERED,
    if 4xx response status code: change message status to UNDELIVERED
    if any errors - do notheing (retry in next cycle iteration)
    """

    url = settings.SEND_SERVICE_URL_TEMPLATE.format(msg_id=message.pk)
    headers = {
        "Authorization": f"Bearer {settings.MAILING_SERVICE_TOKEN}",
    }
    data = {
        "id": message.pk,
        "phone": int(client.phone),
        "text": mailing.text,
    }
    try:
        async with aiohttp.ClientSession(headers=headers) as session:
            async with session.post(
                url, data=json.dumps(data).encode("utf-8")
            ) as response:
                text = await response.text()
                if response.status // 100 == 2:  # if success
                    logger.debug(
                        f"{mailing.pk=} {client.pk=} {message.pk=} {response.status=} '{text}'"
                    )
                    message.status = message.DELIVERED
                    await database_sync_to_async(Message.save)(message)
                elif response.status // 100 == 4:  # if client error
                    logger.warning(
                        f"{mailing.pk=} {client.pk=} {message.pk=} {response.status=} '{text}'"
                    )
                    message.status = message.UNDELIVERED
                    await database_sync_to_async(Message.save)(message)
                # if there is any server error (status=500 for example) message will be sended again
    except aiohttp.ClientError as e:
        logger.warning(
            f"{mailing.pk=} {client.pk=} {message.pk=} exception while send request {type(e)=} {e=}"
        )


async def try_to_send_messages():
    messages = await database_sync_to_async(Message.objects.filter)(
        status=Message.WAITING
    )
    messages = await database_sync_to_async(list)(messages)
    tasks = []
    for message in messages:
        mailing = await database_sync_to_async(getattr)(message, "mailing")
        client = await database_sync_to_async(getattr)(message, "client")
        if timezone.now() < mailing.start_datetime:
            continue
        if timezone.now() > mailing.end_datetime:
            logger.warning(f"{message.id} too late for sending")
            message.status = message.UNDELIVERED
            await database_sync_to_async(Message.save)(message)
            continue
        if not is_time_awailable(mailing, client):
            continue
        task = asyncio.create_task(try_to_send_message(message, client, mailing))
        tasks.append(task)
    for task in tasks:
        await task


async def main():
    while True:
        await try_to_send_messages()
        await asyncio.sleep(settings.MESSAGES_CHECK_COOLDOWN)


class Command(BaseCommand):
    help = "Searching for messages and send them if they should be"

    def handle(self, *args, **kwargs):
        asyncio.run(main())
