from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
)

from .models import Mailing, Client, Message


class MailingSerializer(ModelSerializer):
    all_messages = SerializerMethodField(read_only=True)
    waiting_messages = SerializerMethodField(read_only=True)
    delivered_messages = SerializerMethodField(read_only=True)
    undelivered_messages = SerializerMethodField(read_only=True)

    def get_all_messages(self, instance):
        return instance.messages.all().count()

    def get_waiting_messages(self, instance):
        return instance.messages.filter(status=Message.WAITING).count()

    def get_delivered_messages(self, instance):
        return instance.messages.filter(status=Message.DELIVERED).count()

    def get_undelivered_messages(self, instance):
        return instance.messages.filter(status=Message.UNDELIVERED).count()

    class Meta:
        model = Mailing
        fields = [
            "id",
            "start_datetime",
            "end_datetime",
            "text",
            "mobile_operator_code_filter",
            "tag_filter",
            "all_messages",
            "waiting_messages",
            "delivered_messages",
            "undelivered_messages",
        ]


class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = [
            "id",
            "phone",
            "mobile_operator_code",
            "tag",
            "timezone",
        ]


class MessageSerializer(ModelSerializer):
    class Meta:
        model = Message
        fields = [
            "id",
            "created_at",
            "status",
            "mailing",
            "client",
        ]


class MailingDetailsSerializer(MailingSerializer):
    messages = MessageSerializer(many=True, read_only=True)

    class Meta(MailingSerializer.Meta):
        fields = MailingSerializer.Meta.fields + ["messages"]
