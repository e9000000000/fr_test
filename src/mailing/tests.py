import asyncio
from unittest.mock import patch

from django.test import TransactionTestCase
from django.utils import timezone
from django.conf import settings
from aiohttp import ClientError

from .management.commands import sender
from .models import Mailing, Client, Message
from .utils import process_mailings


class FakeLogger:
    def debug(*args):
        pass

    def warning(*args):
        pass


class FakeResponse:
    def __init__(self, status, text):
        self.status = status
        self._text = text

    def __repr__(self):
        return str(self.status)

    async def text(self):
        return self._text

    async def __aenter__(self):
        return self

    async def __aexit__(self, *args):
        pass


responses = [FakeResponse(200, "")]
attempts_to_send = 0


class FakeClientSession:
    def __init__(self, headers):
        self.headers = headers

    async def __aenter__(self):
        return self

    async def __aexit__(self, *args):
        pass

    def post(self, url, data):
        global attempts_to_send

        attempts_to_send += 1
        r = responses.pop(0)
        if r is None:
            raise ClientError("")
        return r


class TestSender(TransactionTestCase):
    def setUp(self):
        self.old_timezone = settings.TIME_ZONE
        settings.TIME_ZONE = "UTC"

    def tearDown(self):
        settings.TIME_ZONE = self.old_timezone

    @patch(
        "mailing.management.commands.sender.aiohttp.ClientSession", FakeClientSession
    )
    def test_wrong_time(self):
        attempts_to_send = 0
        mailing = Mailing(
            start_datetime=timezone.now() - timezone.timedelta(days=2),
            end_datetime=timezone.now() + timezone.timedelta(days=2),
            awailable_time_start=(timezone.now() - timezone.timedelta(hours=1)).time(),
            awailable_time_end=(timezone.now() + timezone.timedelta(hours=1)).time(),
            text="feewf",
            mobile_operator_code_filter="12",
            tag_filter="12",
        )
        mailing.save()

        lated_mailing = Mailing(
            start_datetime=timezone.now() - timezone.timedelta(days=4),
            end_datetime=timezone.now() - timezone.timedelta(days=2),
            awailable_time_start=(timezone.now() - timezone.timedelta(hours=1)).time(),
            awailable_time_end=(timezone.now() + timezone.timedelta(hours=1)).time(),
            text="feewf",
            mobile_operator_code_filter="12",
            tag_filter="13",
        )
        lated_mailing.save()

        too_early_mailing = Mailing(
            start_datetime=timezone.now() + timezone.timedelta(days=2),
            end_datetime=timezone.now() + timezone.timedelta(days=4),
            awailable_time_start=(timezone.now() - timezone.timedelta(hours=1)).time(),
            awailable_time_end=(timezone.now() + timezone.timedelta(hours=1)).time(),
            text="feewf",
            mobile_operator_code_filter="12",
            tag_filter="13",
        )
        too_early_mailing.save()

        client = Client(
            phone="71111111112",
            mobile_operator_code="12",
            tag="12",
            timezone="Europe/Moscow",  # UTC+3
        )
        client.save()

        client2 = Client(
            phone="71111111112", mobile_operator_code="12", tag="13", timezone="UTC"
        )
        client2.save()

        process_mailings()
        asyncio.run(sender.try_to_send_messages())
        messages = list(Message.objects.all())
        self.assertEqual(len(messages), 3)
        self.assertEqual(list(mailing.messages.all())[0].status, Message.WAITING)
        self.assertEqual(
            list(lated_mailing.messages.all())[0].status, Message.UNDELIVERED
        )
        self.assertEqual(
            list(too_early_mailing.messages.all())[0].status, Message.WAITING
        )
        self.assertEqual(attempts_to_send, 0)
        attempts_to_send = 0

    @patch(
        "mailing.management.commands.sender.aiohttp.ClientSession", FakeClientSession
    )
    def test_right_client_time(self):
        responses = [
            FakeResponse(100, "{{ }}"),
            FakeResponse(300, "{{ }}"),
            FakeResponse(500, "{{ }}"),
            None,
            FakeResponse(200, "{{ }}"),
        ]
        mailing = Mailing(
            start_datetime=timezone.now() - timezone.timedelta(days=2),
            end_datetime=timezone.now() + timezone.timedelta(days=2),
            awailable_time_start=(timezone.now() - timezone.timedelta(hours=1)).time(),
            awailable_time_end=(timezone.now() + timezone.timedelta(hours=1)).time(),
            text="feewf",
            mobile_operator_code_filter="12",
            tag_filter="12",
        )
        mailing.save()

        client = Client(
            phone="71111111111", mobile_operator_code="12", tag="12", timezone="UTC"
        )
        client.save()

        process_mailings()
        for _ in range(len(responses)):
            asyncio.run(sender.try_to_send_messages())

        messages = list(Message.objects.all())
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0].status, Message.DELIVERED)
