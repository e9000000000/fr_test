import logging

from .models import Mailing, Client, Message


logger = logging.getLogger("mailing")


def process_mailings():
    """register all specified in mailings messages for sending to clients"""

    for mailing in Mailing.objects.all():
        process_mailing(mailing)


def process_mailing(mailing: Mailing):
    """register all specified in mailing messages for sending to clients"""

    logger.debug(f"{mailing.pk=} processing started")
    clients = Client.objects.filter(
        mobile_operator_code=mailing.mobile_operator_code_filter,
        tag=mailing.tag_filter,
    )
    for client in clients:
        message = Message(mailing=mailing, client=client)
        message.save()
        logger.debug(f"{mailing.pk=} {client.pk=} {message.pk=} message created")
    logger.debug(f"{mailing.pk=} processing ended")
