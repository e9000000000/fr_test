# fr_test


## how to run
set up env variables
- `SECRET_KEY` - django secret key
- `MAILING_SERVICE_TOKEN` - jwt token for mailing service
- `POSTGRES_USER` - postgres user name
- `POSTGRES_PASSWORD` - strong password to postgres
- `MAILING_DB_NAME` - data base name

### test
run docker compose
```bash
docker-compose -f docker-compose-test.yml up --build --abort-on-container-exit
```

### run
run docker compose
```bash
docker-compose up --build
```

if database is not created:
```bash
docker exec -it postgres /bin/bash
psql --user=postgres
CREATE DATABASE your_db_name;
```

## docs
for swagger ui: run docker-compose, then open in browser `http://localhost:8000/docs`  
for openapi: open `http://localhost:8000/docs/?format=openapi` or view it in /docs dir

## additional tasks
1. completed
2. X
3. completed
4. X
5. completed
6. X
7. X
8. X
9. completed
10. X
11. completed
12. completed
