FROM python:latest AS base

WORKDIR /app

COPY requirements.txt ./
RUN pip install --upgrade pip \
&& pip install -r requirements.txt

FROM base AS prod
CMD ./manage.py makemigrations && \
./manage.py migrate --run-syncdb && \
echo "yes" | ./manage.py collectstatic && \
./manage.py sender & \
gunicorn --reload -b 0.0.0.0:8000 -w 4 mysite.wsgi

FROM base AS test
CMD ./manage.py test
